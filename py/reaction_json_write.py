import _pickle as pickle
import json
import re
import sys
import string
import os

#from string import maketrans
PP_PATH = '/mnt/home/herman67/clean_build/'

arrayfile = PP_PATH + 'py/nuc_pair'

isWeak = False
isInverse = False
isDecay = False

with open(arrayfile,'rb') as f1:
  pair = pickle.load(f1)

z_std = []
name_std = []

for x in pair:
  z_std.append(x[0])
  name_std.append(x[1])


reaction_check = []
reac_name_ref = []
reaction_dict = {}

count = 0
for x in range(100):
  filename = PP_PATH + 'outputs_base/run_sep12_'+str(x)+'/0.reactions.reduced1e0'
  if not os.path.isfile(filename):
    continue
  with open(filename, 'r') as f:
    for line in f:
      if(line.split(',')[1] not in reaction_check):
        count += 1
        print(count)
        reaction_check.append(line.split(',')[1])
        print('New reaction found in ' + filename)
        linev = line.split(',')
        nuc_temp = linev[0]
        reac_temp = linev[1]
        rate1 = float(linev[2])
        rate2 = float(linev[5])
        equals = 0
        reaction = []
        reactants = []
        products = []
        reactantsN = []
        productsN = []
        reaction_name = ''
        for x in reac_temp.split():
          if(x =='+'):
            continue
          elif(x == '->'):
            equals = len(reaction)
            continue
          else:
            reaction.append(x)  
          if equals == 0:
            if x in reactants:
              reactantsN[reactants.index(x)] = reactantsN[reactants.index(x)] + 1
            else:
              reactants.append(x)
              reactantsN.append(1)
              reaction_name = reaction_name + '-' + x
          elif equals > 0:
            if x in products:
              productsN[products.index(x)] = productsN[products.index(x)] + 1
            else:
              products.append(x)
              productsN.append(1)
              reaction_name = reaction_name + '-' + x

        multiplier = 1.0
        reaction_dict.update({reaction_name : { 'reactants' : reactants, 'products' : products, 
                    'reactantsN' : reactantsN, 'productsN' : productsN, 
                    'isWeak' : isWeak, 'isInverse' : isInverse, 'isDecay' : isDecay, 
                    'label' : reaction_name, 'multiplier' : multiplier } } ) 
        reac_name_ref.append(reaction_name)

print("end for loop")

with open(PP_PATH + 'test.json', 'w') as out:
  json.dump( reaction_dict , out , indent=2 )

with open(PP_PATH + '/variances_tot_orig/json_output0.json','r') as old:
    old_json = json.load(old)

for i in old_json:
    if not( i in reaction_dict.keys()):
        print("used to be in: ", i)

for j in reaction_dict:
    if not (j in old_json.keys()):
        print("is now added: ", j)


multiplier = 100.0
n_reacs = len(reaction_dict) 

print(n_reacs)
with open(PP_PATH + 'variances_tot_'+str(int(multiplier))+'/json_output'+str(0)+'.json', 'w') as out:
  json.dump( reaction_dict , out , indent=2 )

for x in range(len(reac_name_ref)):
#  print(str(x)+" "+str(reaction[reac_name_ref[x]]))
  reaction_dict[reac_name_ref[x]].update({ 'multiplier' : multiplier })
#  print(reaction_dict[reac_name_ref[x]])
  with open(PP_PATH + 'variances_tot_'+str(int(multiplier))+'/json_output'+str(x+1)+'.json', 'w') as out:
    temp_dict = { reac_name_ref[x] : reaction_dict[reac_name_ref[x]] }
    json.dump( temp_dict , out , indent=2 )
  reaction_dict[reac_name_ref[x]].update({ 'multiplier' : 1/multiplier })
#  print(reaction_dict[reac_name_ref[x]])
#  reaction_dict[reac_name_ref[x]].update({ 'multiplier' : 0.5 })
  with open(PP_PATH + 'variances_tot_'+str(int(multiplier))+'/json_output'+str(x+1+n_reacs)+'.json', 'w') as out:
    temp_dict = { reac_name_ref[x] : reaction_dict[reac_name_ref[x]] }
    json.dump( temp_dict , out , indent=2 )

  reaction_dict[reac_name_ref[x]].update({ 'multiplier' : 1.0 })
            
print(len(reaction))
  
#  with open('reaction_list', 'w') as f:
#    for x in reaction:
#      f.write(x)
#      f.write('\n')
