#!/usr/bin/env python3

import h5py

from matplotlib import pyplot as py
import pandas as pd
import numpy as np
from scipy.interpolate import interp1d
import multiprocessing as mp
import os
# take command line arguments
    # first is the reac_vary number
    # second is the final time

PP_PATH = '../'
PP_OUT = PP_PATH + "outputs_100/"

OUTPUT = PP_OUT 

CSV_OUT = PP_PATH + "csv_outs_test/"
if not os.path.isdir(CSV_OUT):
    os.makedirs(path_temp_output)
    
number_of_tracers = 200


# function to read the outputs for all tracers and sum the mass fractions at the final time
def read_table( OUTPUT_FOLDER, reac_id, time):

    filename = OUTPUT_FOLDER+'/run_sep12_'+str(0)+'/'+ reac_id + '_2.h5'
    f = h5py.File(filename,'r')
    data_Z = np.array(f['Z'], dtype=int)
    data_A = np.array(f['A'], dtype=int)

    total_mass = 0
    data_N, data_X, data_Y, data_M = data_A-data_Z, np.zeros(len(data_A)), np.zeros(len(data_A)), np.zeros(len(data_A))
    for j in range(number_of_tracers):
        # dm for inner tracers 
        dm = 0.00453703704
        # dm for outer tracers (which don't evolve)
        if j > 99:
            dm = 0.089618957999258
        total_mass += dm
        filename = OUTPUT_FOLDER + '/run_sep12_'+str(j)+"/"+ reac_id + '_2.h5'
        f = h5py.File(filename,'r')
        time_fixed = np.array(f['Time'])
        Y = np.array(f['Y'])
        f_time = interp1d(time_fixed, Y, axis=0)
        data_Y = f_time(time)
        temp_X = np.multiply(data_Y, data_A)
        data_M += np.multiply(temp_X, dm)
        

    data_X = np.multiply(data_M, 1/(np.sum(data_M)))
    print(np.sum(data_X))

    data = { 'Z' : data_Z, 'A' : data_A, 'N': data_N, 'X' : data_X, 'total_mass' : data_M }
    df = pd.DataFrame(data)
    return df

def parallel_write( j ):

    # time to read evolutions at (s)
    time = 1e15 
    df = read_table( OUTPUT, str(j), time)
    df.to_csv(CSV_OUT+str(j)+".csv")

    return    

def main():

    number_of_processes = 1

    reac_ids = [np.loadtxt(PP_PATH + 'reactions_all.txt', dtype=int)]
#    print(reac_ids[0], reac_ids[-1], len(reac_ids))
    pool = mp.Pool(number_of_processes)
    pool.map(parallel_write, reac_ids)
    pool.close()
    pool.join()

    return

if __name__=='__main__':
    main()
