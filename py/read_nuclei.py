import re

PP_PATH = '../'

def read_nuclei(nuclei):
    nucf =  open(PP_PATH+'inputs/nuclide_z_name_index','r')

    nuclei = nuclei.lower()    
    nucname = []
    z = []
    i = 0    
    for line in nucf:
        if ( i>0 and i<121 ):
            if(i == 1):  
                nucname.append('neut')
            elif(i == 2):  
                nucname.append('prot')
            else:
                nucname.append(line.split()[0])
            z.append(line.split()[1])
        i = i + 1
    nuclei_name = re.findall('\D+', nuclei)[0]

    try:
        nuclei_a = re.findall('\d+', nuclei)[0]

    except IndexError:
        if nuclei == 'n':
            nuclei_a = 1
            nuclei_z = 0
        elif nuclei == 'p' or nuclei == "1H":
            nuclei_a = 1
            nuclei_z = 1
        elif nuclei == 'd' or nuclei == "2H":
            nuclei_a = 2 
            nuclei_z = 1
        elif nuclei == 't' or nuclei == "3H":
            nuclei_a = 3 
            nuclei_z = 1
        else:
            raise Exception("'" + nuclei + "'" + " has no mass number")

    if nuclei_name in nucname  :
        nuclei_z = z[nucname.index(nuclei_name)]
    else:
        print(nucname)
        if nuclei_name == 'h' or nuclei_name == "H":
            nuclei_z = 1
        else:
            raise Exception("'" + nuclei_name + "'" + " not found in nuclide file")
    return int(nuclei_z), int(nuclei_a)

def main():
    print(read_nuclei('ti44'))
    print(read_nuclei('p'))
    print(read_nuclei('n'))
#    print(read_nuclei('h3'))
    return     

if __name__=='__main__':

    main()
