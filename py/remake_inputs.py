import pandas as pd
from scipy.interpolate import interp1d
import re
import pickle


def find_nuclei_from_z(z,nuc_dict):
    return list(nuc_dict.keys())[list(nuc_dict.values()).index(z)]

DATA_DIR = '../outside_data/'

# read s12.0 table of input mass fractions
df = pd.read_csv('../inputs/s12.0_presn-edit',delim_whitespace=True)
df = pd.read_csv('../outside_data/s15a27.presn_comp',delim_whitespace=True)
df['solar_mass'] = df['mass-below'] / 1.98847e33
df = pd.read_csv('../outside_data/12.0.full.ktbl',delim_whitespace=True,skiprows=3)
df['solar_mass'] = 0.0
running_mass = 0.0
for i in range(df.shape[0]):
    df['solar_mass'][i] = float(df['mass'][i]) / 1.98847e33 + running_mass
    running_mass = float(df['solar_mass'][i])
    print(running_mass)
#df['solar_mass'] = df['cell-outer-total-mass'] / 1.98847e33
header = list(df.head())
#print(header[14:-2])
nuclei_header = header[14:-3]
nuclei_header = header[12:-2]
print(nuclei_header)
print(len(nuclei_header))
df_solar = pd.read_csv(DATA_DIR + 'lodders-mass-fractions.csv')
df_add = df_solar.loc[df_solar['include']==0]
nuc_dict = pickle.load(open('./nuc_dict','rb'))
#print(nuc_dict)

for i in range(0,200):
    str_i = str(i)
    traj = '../inputs/Lagrangian_nu/stir2_oct8_s12.0_alpha1.25_tracer'+str_i+'.dat'
    traj_mass = 0.0
    with open(traj, 'r') as f:
        line_no = 1
        for line in f:
            line_split = line.split()
            traj_mass = float(line_split[3])
            break
    with open(traj+'.inp','w') as f:
        for j in range(len(nuclei_header)):
            mass_interp = interp1d(df['solar_mass'], df[nuclei_header[j]])
            nuclei = nuclei_header[j].lower()
            nuclei_a = 0
            nuclei_z = 0
#            if nuclei == 'neutrons':
            if nuclei == 'nt1':
                nuclei = 'n' 
                nuclei_a = 1
                nuclei_z = 0
            else:
#                print(nuclei)
                nuclei_a = int(re.findall(r'\d+', nuclei)[0])
                nuclei_name = str(re.findall(r'\D+', nuclei)[0])
                nuclei_z = nuc_dict[nuclei_name]
            nuclei_n = nuclei_a - nuclei_z
            abundance_out = mass_interp(traj_mass)/nuclei_a
            if nuclei_n >= 70 or nuclei_z >= 50:
                continue
            f.write(nuclei + " " + str(abundance_out) + "\n") 

# uncomment if want to add solar abundances
#   adds abundances specified with "include == 0" in lodders-mass-fractions.csv

#        for j in df_add.index:
#            z = int(df_add["Z"][j])
#            nuclei_a = df_add["A"][j]
#            n = nuclei_a - z
#            if n >= 70 or z >= 50:
#                continue
#            nuclei_name = nuc_dict[z][1]
#            abundance_out = df_add["X"][j]/nuclei_a             
#            nuclei = nuclei_name + str(nuclei_a)
#            f.write(nuclei + " " + str(abundance_out) + "\n") 
#


