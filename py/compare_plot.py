import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import read_nuclei 

PP_PATH="../"


def make_plot_arrays(df, df_solar):
    X_total = df['total_mass']/np.sum(df['total_mass'])
    solar_A, solar_ratio = [], []
    for i in range(df.shape[0]):
        for j in range(df_solar.shape[0]):
            if df['Z'][i] == df_solar["Z"][j] and df["A"][i] == df_solar["A"][j]:
                solar_ratio.append(X_total[i] / df_solar["X"][j])
                solar_A.append(df["A"][i])
    return [solar_A, solar_ratio ] 

csv_outs_test = '../csv_outs_test/'
outside_data_dir = '../outside_data/'

# read in stir data
df = pd.read_csv(csv_outs_test + '0.csv')
# read in solar data
df_solar = pd.read_csv(outside_data_dir+'lodders-mass-fractions.csv')

# if want to only show nuclei produced in large amounts
# df_solar = df_solar.loc[df_solar["X"]*df_solar["A"] > 1e-4]
# df_solar = df_solar.reset_index(drop=True)

# make stir plot arrays
stir_solar_A, stir_solar_ratio = make_plot_arrays( df , df_solar )

# read in sukhbold data
df_suk = pd.read_csv(outside_data_dir + 's12.0.yield_table-edit',delim_whitespace=True)
df_suk["Z"] = int(0)
df_suk["A"] = int(0)
df_suk["sumif"] = False
for i in range(df_suk.shape[0]):
    Z, A = read_nuclei.read_nuclei(df_suk['[isotope]'][i])
    df_suk['Z'][i] = Z
    df_suk["A"][i] = A
# set key to the one used for make_plot_arrays
df_suk['total_mass']=df_suk['[ejecta]']

# make sukhbold plot arrays
suk_solar_A, suk_solar_ratio = make_plot_arrays( df_suk , df_solar )

# read in push data
df_push = pd.read_csv(outside_data_dir+'push_wdata_reduced.csv')
df_push["A"] = int(0)  
df_push["Z"] = int(0) 
for i in range(df_push.shape[0]):
    Z, A = read_nuclei.read_nuclei(df_push["nuclei"][i])  
    df_push["A"][i] = A 
    df_push["Z"][i] = Z 

# make push arrays
push_solar_A, push_solar_ratio = make_plot_arrays( df_push , df_solar )


# plot results
plt.plot(stir_solar_A, np.zeros(len(stir_solar_A))+1.0, "k-")
plt.plot(stir_solar_A, stir_solar_ratio, "x", label="STIR")
plt.plot(suk_solar_A, suk_solar_ratio, "+", label="Sukhbold")
plt.plot(push_solar_A, push_solar_ratio, "o", fillstyle="none", label="PUSH")
plt.ylabel('ratio to solar')
plt.xlabel('A')
plt.yscale('log')
plt.xlim(0,80)
plt.ylim(1e-2,1e2)
plt.legend()
plt.savefig('ratio_to_solar')
plt.show() 
