#!/bin/bash
# Runs with $ sh quickrun.sh # # # ...
#
# Compile code.
#
# make
#
# Execute tests.
#
# echo -e "version\tn\tmax\tl2norm\ttime" 

cd ./build
for TOSSES in $(seq 0 1 99) 
do
  ./fulltraj "$TOSSES" 0
done

