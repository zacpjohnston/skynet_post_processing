#include <math.h>
#include <memory> 
#include <algorithm> 
#include <iostream> 
#include <string> 
#include <fstream> 
#include <sstream> 

#include "BuildInfo.hpp"
#include "EquationsOfState/HelmholtzEOS.hpp"
#include "EquationsOfState/SkyNetScreening.hpp"
#include "Reactions/REACLIBReactionLibrary.hpp"

#include "../src/MultiplierReactionLibrary.hpp"

int main(int /*narg*/, char** /*args*/) {
  
  auto nuclib = NuclideLibrary::CreateFromWebnucleoXML(
      SkyNetRoot + "/data/webnucleo_nuc_v2.0.xml", 
      {"p", "n",  "he4", "ti44", "ti45", "v45", "v47"});

  // Make reactions to vary 
  auto Ti44V47 = Reaction({"ti44", "he4"}, {"p", "v47"}, {1, 1}, {1, 1}, 
      false, false, false, "ti44v47", nuclib);
  auto Ti44V45 = Reaction({"ti44", "p"}, {"v45"}, {1, 1}, {1}, 
      false, false, false, "ti44v45", nuclib);
  
  NetworkOptions opts;
  MultiplierReactionLibrary<REACLIBReactionLibrary> strongReactionLibrary(
        REACLIBReactionLibrary(SkyNetRoot + "/data/reaclib",
        ReactionType::Strong, true, LeptonMode::TreatAllAsDecayExceptLabelEC,
        "Strong reactions", nuclib, opts, false));
  
  for (auto& reac : strongReactionLibrary.Reactions().AllData()) 
      std::cout << reac.String() << " " << reac.Label() << std::endl;
  
  SkyNetScreening screen(nuclib);
  HelmholtzEOS helm(SkyNetRoot + "/data/helm_table.dat");

  double T9 = 1.0; 
  std::vector<double> Y(nuclib.NumNuclides(), 0.0); 
  Y[0] = 1.0;
  ThermodynamicState state = helm.FromTAndRho(T9, 1.e4, Y, nuclib);  
  std::vector<double> partFuncs = nuclib.PartitionFunctionsWithoutSpinTerm(T9);

  std::cout << std::endl << "Unvaried: " << std::endl;
  strongReactionLibrary.CalculateRates(state, partFuncs, 1.e4, NULL, NULL); 
  for (unsigned int i=0; i < strongReactionLibrary.Rates().size(); ++i)
    std::cout << strongReactionLibrary.Reactions().ActiveData()[i].String() 
        << " " << strongReactionLibrary.Rates()[i] << std::endl; 
  
  std::cout << std::endl << "Varied: " << std::endl;
  strongReactionLibrary.SetRateMultipliers({Ti44V45, Ti44V47}, {2.0, 10.0});
  strongReactionLibrary.CalculateRates(state, partFuncs, 1.e4, NULL, NULL); 
  for (unsigned int i=0; i < strongReactionLibrary.Rates().size(); ++i)
    std::cout << strongReactionLibrary.Reactions().ActiveData()[i].String() 
        << " " << strongReactionLibrary.Rates()[i] << std::endl; 
  
  return 0;

}

